import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core'
import { OrderService } from '@app/services/order.service';

@Component({
  selector: 'app-recent-orders-list',
  templateUrl: './recent-orders-list.component.html',
  styleUrls: ['./recent-orders-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RecentOrdersListComponent implements OnInit {
  public recentOrders: string[];

  constructor(private orderService: OrderService) { }

  ngOnInit() {
    this.recentOrders = this.orderService.getRecentOrders();
  }
}
