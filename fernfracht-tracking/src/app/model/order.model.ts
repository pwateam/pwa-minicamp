export class Location {
    Position: {
        Latitude: number;
        Longitude: number;
      };
    Address: any
}

export class LoadingWindow {
    public Start: string;
    public End: string;
}

export class LoadingTask {
    public Id: number;
    public LoadId: number;
    public TaskType: string;
    public SequenceNumber: number;
    public LoadingWindows: LoadingWindow[];
    public LoadingInstructions: string;
    public SpecialInstructions: string;
    public WithPalletExchange: boolean;
    public Status: string;
    public CustomerOrderNumber: string;
    public Location: Location;
    public LoadDescription: string;
}

export class Load {
    public Id: number;
    public CustomerOrderNumber: string;
    public LoadDescription: string;
    public Reports: any;
}

export class Order {
    public Id: string;
    public PositionNumber: string;
    public Status: string;
    public LoadingTasks: LoadingTask[];
}