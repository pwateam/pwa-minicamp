import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';

import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatListModule} from '@angular/material/list';
import {MatExpansionModule} from '@angular/material/expansion';
import {FormsModule} from '@angular/forms'

import { ServiceWorkerModule } from '@angular/service-worker'

import { LoadOrderFormComponent } from './load-order-form/load-order-form.component';
import { RecentOrdersListComponent } from './recent-orders-list/recent-orders-list.component';
import { OrderOverviewComponent } from './order-overview/order-overview.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { OrderService } from './services/order.service';

import { HttpModule } from '@angular/http';
import { environment } from 'environments/environment';

const appRoutes: Routes = [
  { path: 'landing', component: LandingPageComponent },
  { path: 'order/:orderId', component: OrderOverviewComponent },
  { path: '',
    redirectTo: '/landing',
    pathMatch: 'full'
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LoadOrderFormComponent,
    RecentOrdersListComponent,
    OrderOverviewComponent,
    LandingPageComponent,
    PageNotFoundComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatListModule,
    MatExpansionModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    environment.production ? ServiceWorkerModule.register('/ngsw-worker.js') : []
  ],
  providers: [OrderService],
  bootstrap: [AppComponent]
})
export class AppModule { }
