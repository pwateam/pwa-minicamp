import { Component, OnInit, ViewEncapsulation, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Order } from '@app/model/order.model';
import { OrderService } from '@app/services/order.service';

@Component({
  selector: 'app-order-overview',
  templateUrl: './order-overview.component.html',
  styleUrls: ['./order-overview.component.css']
})
export class OrderOverviewComponent implements OnInit {
  public orderId: string;
  public order: Order;
  public error: any;
  public pinRequired: boolean = false;
  public pin: string;

  constructor(private route: ActivatedRoute, private orderService: OrderService) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.orderId = params.get("orderId");
      this.loadOrder();
    });
  }

  public loadOrder() {
    this.orderService.get(this.orderId, this.pin).subscribe(
      order => { 
        this.pinRequired = false;
        this.order = order; 
      },
      error => {
        if (error.status == 401) {
          this.pinRequired = true;
        } else {
          this.pinRequired = false;
          this.error = error;
        }
      });
  }
}
