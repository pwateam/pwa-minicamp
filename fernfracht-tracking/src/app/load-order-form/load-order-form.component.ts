import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core'
import { Router } from '@angular/router';

@Component({
  selector: 'app-load-order-form',
  templateUrl: './load-order-form.component.html',
  styleUrls: ['./load-order-form.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class LoadOrderFormComponent implements OnInit {
  public orderId: string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public loadOrder() {
    this.router.navigateByUrl('order/' + this.orderId);
  }
}
