import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadOrderFormComponent } from './load-order-form.component';

describe('LoadOrderFormComponent', () => {
  let component: LoadOrderFormComponent;
  let fixture: ComponentFixture<LoadOrderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadOrderFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadOrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
