import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { Order } from '@app/model/order.model';
import { Observable } from 'rxjs/Observable';
import { Http, RequestOptions, Headers } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class OrderService {
  constructor(private http: Http) { }

  public get(id: string, pin?: string, driver?: string): Observable<Order> {
    let url = `${environment.trackingApiEndpoint}/orders/${id}`;
    let requestOptions = new RequestOptions();
    requestOptions.headers = new Headers();
    
    if (!pin) {
      let recentOrders = this.getRecentOrdersWithPin() as Map<string, string>;
      if (recentOrders != null && recentOrders.has(id)){
        pin = recentOrders.get(id);
      }
    }

    if (pin || driver) {
      requestOptions.headers.append('Authorization', 'Basic ' +  btoa(driver + ':' + pin));
    }

    return this.http.get(url, requestOptions)
      .map(response => {
        let order = response.json() as Order;
        this.saveRecentOrder(order.Id, pin);
        return order;
      });
  }

  private saveRecentOrder(id: string, pin: string) {
    let recentOrders = this.getRecentOrdersWithPin();
    recentOrders.set(id, pin);
    localStorage.setItem('recentOrders', JSON.stringify([...Array.from(recentOrders.entries())]));
  }

  private getRecentOrdersWithPin(): Map<string, string>{
    let storedRecentOrders = localStorage.getItem('recentOrders')

    if (!storedRecentOrders || storedRecentOrders == '{}') {
      return new Map<string, string>();
    }

    let recentOrdersObject = JSON.parse(storedRecentOrders);
    return new Map<string, string>(recentOrdersObject);
  }

  public getRecentOrders(): string[] {
    let recentOrders = this.getRecentOrdersWithPin();
    return Array.from(recentOrders.keys());
  }
}
