# PWA Mini Camp #

Dokumentation der Ergebnisse des 2-t�gigen MiniCamp zum Thema PWA.

### Subprojekte + Beschreibung ###

* "sample_notes_app"
	* Source: [link](https://codelabs.developers.google.com/codelabs/polymer-firebase-pwa/index.html)
	* Lesson Learned: learn to copy !
	* Tools: VS Code, [Firebase](http://firebase.google.com), [Polymer](https://www.polymer-project.org/)
* other
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Implementierte Tutorials bzw. HowTos ###

* https://codelabs.developers.google.com/codelabs/your-first-pwapp
* https://codelabs.developers.google.com/codelabs/polymer-firebase-pwa/index.html
* 

### Resourcen / Links ###

* Useful Links
	* [Manifest Generator](https://tomitm.github.io/appmanifest/)
* Tools
* Other 

### PWAs mit Angular ###

* ServiceWorker: Es gibt ein NPM Paket, dass Service Worker f�r Angular Apps bietet
	* @angular/service-worker
	* �ber ein config-file konfigurierbar
	* Siehe auch https://medium.com/@webmaxru/a-new-angular-service-worker-creating-automatic-progressive-web-apps-part-1-theory-37d7d7647cc7
* Manifest: Handgeschrieben 
	* auf der Basis von https://developer.mozilla.org/en-US/docs/Web/Manifest
	* Sicherstellen dass manifest in assets und index.html enthalten ist
* Web install banners: https://developers.google.com/web/fundamentals/app-install-banners/
* Native App look and feel: https://github.com/angular/flex-layout

### Teilnehmer ###

* Repo owner or admin
* Other community or team contact