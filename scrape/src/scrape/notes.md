# Scrape

Try to scrape [this](https://www.ets.org/gre/revised_general/prepare/analytical_writing/argument/pool) website. Also [this](https://www.ets.org/gre/revised_general/prepare/analytical_writing/issue/pool).

## Observations

    <div class="divider-50"><hr /></div>
    <p>content</p>
    <div class="indented">more content</div>

## Obtaining data

Return JSON for `url/arguments` and `url/issues`.
