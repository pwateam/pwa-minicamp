package main

import (
	"encoding/json"
	"log"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

const (
	ArgumentsUrl = "https://www.ets.org/gre/revised_general/prepare/analytical_writing/argument/pool"
	IssuesUrl    = "https://www.ets.org/gre/revised_general/prepare/analytical_writing/issue/pool"

	RateDelay time.Duration = time.Hour
)

type Item struct {
	Statement string `"json:statement"`
	Task      string `"json:task"`
}

type Scraper struct {
	items []*Item
	mutex sync.RWMutex
	rate  time.Duration
	url   string
}

func NewScraper(url string) *Scraper {
	return &Scraper{
		[]*Item{},
		sync.RWMutex{},
		RateDelay,
		url,
	}
}

func (s *Scraper) Update() error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	doc, err := goquery.NewDocument(s.url)
	if err != nil {
		return err
	}

	s.items = []*Item{}

	doc.Find("#main-contents .divider-50").Each(func(i int, sel *goquery.Selection) {
		statementSel := sel.Next()
		statement := strings.Trim(statementSel.Text(), " \r\n\t")

		taskSel := statementSel.Next()
		if _, ok := taskSel.Attr("class"); !ok {
			statement += "<br />" + taskSel.Text()
			taskSel = taskSel.Next()
		}
		task := strings.Trim(taskSel.Text(), " \r\n\t")

		s.items = append(s.items, &Item{statement, task})
	})

	return nil
}

func (s *Scraper) Run() {
	ticker := time.Tick(s.rate)
	s.Update()

	for range ticker {
		s.Update()
	}
}

func (s *Scraper) Handle(w http.ResponseWriter, r *http.Request) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	json.NewEncoder(w).Encode(s.items)
}

func main() {
	arguments := NewScraper(ArgumentsUrl)
	go arguments.Run()

	issues := NewScraper(IssuesUrl)
	go issues.Run()

	router := mux.NewRouter()
	router.HandleFunc("/arguments", arguments.Handle)
	router.HandleFunc("/issues", issues.Handle)

	log.Fatal(http.ListenAndServe(":8080", handlers.CORS()(router)))
}
