package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

const (
	TopUrl = "https://www.aktionsfinder.at/top-angebote.html"

	RateDelay time.Duration = time.Hour
)

type Item struct {
	Title     string `"json:title"`
	Price     string `"json:price"`
	Reduction string `"json:reduction"`
}

type Scraper struct {
	items []*Item
	mutex sync.RWMutex
	rate  time.Duration
	url   string
}

func NewScraper(url string) *Scraper {
	return &Scraper{
		[]*Item{},
		sync.RWMutex{},
		RateDelay,
		url,
	}
}

func (s *Scraper) Update() error {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	// no scraping during development
	//doc, err := goquery.NewDocument(s.url)
	//if err != nil {
	//	return err
	//}
	f, err := os.Open("top-angebote.html")
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()
	doc, err := goquery.NewDocumentFromReader(f)
	if err != nil {
		log.Fatal(err)
	}

	s.items = []*Item{}

	doc.Find(".top-angebote .item-wrapper").Each(func(i int, sel *goquery.Selection) {
		var title string
		// title
		if titleSel := sel.Find(".headline a").First(); titleSel != nil {
			title, _ = titleSel.Attr("title")
		}

		// price
		var price string
		if priceSel := sel.Find(".price-box .price"); priceSel != nil {
			price = strings.Trim(priceSel.Text(), " \r\n\t")
		}

		// reduction
		var reduction string

		if reductionSel := sel.Find(".price-box .info-left"); reductionSel != nil {
			reduction = strings.Trim(reductionSel.Text(), " \r\n\t")
		}

		s.items = append(s.items, &Item{title, price, reduction})
	})

	return nil
}

func (s *Scraper) Run() {
	ticker := time.Tick(s.rate)
	s.Update()

	for range ticker {
		s.Update()
	}
}

func (s *Scraper) Handle(w http.ResponseWriter, r *http.Request) {
	s.mutex.RLock()
	defer s.mutex.RUnlock()

	json.NewEncoder(w).Encode(s.items)
}

func main() {
	top := NewScraper(TopUrl)
	go top.Run()

	router := mux.NewRouter()
	router.HandleFunc("/top", top.Handle)

	log.Fatal(http.ListenAndServe(":8080", handlers.CORS()(router)))
}
