var CACHE_NAME = 'af-cache';
var FILES = [
    '/',
    '/index.html',
    '/script.js',
];
self.addEventListener('install', function (event) {
    console.log('installing serviceworker event listener');
    event.waitUntil(caches
        .open(CACHE_NAME)
        .then(function (cache) {
        console.log('cache added');
        return cache.addAll(FILES);
    }));
});
self.addEventListener('activate', function (event) {
    console.log('activating service worker');
    event.waitUntil(caches
        .keys()
        .then(function (keys) {
        return Promise.all(keys
            .filter(function (key) { return key != CACHE_NAME; })
            .map(function (key) { return caches["delete"](key); }));
    }));
});
