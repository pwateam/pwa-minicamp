let CACHE_NAME: string = 'af-cache';
let FILES: string[] = [
	'/',
	'/index.html',
	'/script.js',
];

interface ExtendableEvent extends Event {
	waitUntil(fn: Promise<any>): void;
}

self.addEventListener('install', (event: ExtendableEvent) => {
	console.log('installing serviceworker event listener');

	event.waitUntil(
		caches
		.open(CACHE_NAME)
		.then(cache => {
			console.log('cache added');
			return cache.addAll(FILES);
		})
	);
});

self.addEventListener('activate', (event: ExtendableEvent) => {
	console.log('activating service worker');

	event.waitUntil(
		caches
		.keys()
		.then(keys => {
			return Promise.all(
				keys
				.filter(key => key != CACHE_NAME)
				.map(key => caches.delete(key))
			);
		})
	);
});
