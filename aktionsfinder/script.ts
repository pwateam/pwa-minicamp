document.getElementById("debug").onclick = () => {
	fetch('http://localhost:8080/top')
		.then(response => {
			console.log('successfully fetched');
			return response.json();

			/*			return response.text();

			response.json()
				.then((obj: JSON) => {
					document.body.innerHTML += obj[0].title + obj[0].price;
				}).catch(error => {
					console.log('failed to receive json: ' + error);
				});
		}).then(text => {
			document.body.innerHTML += text;*/
		}).then(obj => {
			let ulElem = document.createElement("ul");

			obj.forEach(element => {
				let liElem = document.createElement("li");
				let innerUlElem = document.createElement("ul");

				let titleElem = document.createElement("li");
				titleElem.innerHTML = element.Title;

				let priceElem = document.createElement("li");
				priceElem.innerHTML = element.Price;

				innerUlElem.appendChild(titleElem);
				innerUlElem.appendChild(priceElem);

				liElem.appendChild(innerUlElem);
				ulElem.appendChild(liElem);
			});
			/*for (let element of obj) {
				let liElem = document.createElement("li");
				liElem.innerHTML = element;
				ulElem.appendChild(liElem);
			}*/
			document.body.appendChild(ulElem);
		}).catch(error => {
			console.log('fetch error: ' + error);
		});
};
